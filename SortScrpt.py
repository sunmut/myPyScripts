# file: SortScrpt.py
# Purp: To walk thro folder files and sort based on Year then Month format.
# Auth: M. Sundaram
# e-id: sundaram.m@zoho.com

# place this file just outside the folder to sort
# eg. --> photos
#     --> SortScrpt.py

# result will be
# --> photos
# --> photos_sort
# --> SortScrpt.py

#to run this script, open in IDLE ide and hit F5

import os
import glob
import os.path
import re
import shutil
import os.path, time
import random


# keep skip True to avoid duplicates
skip = True

# file count
count = 0
# current dir
source = os.getcwd()
#print(source)

# get year
def getYrMon(file_name):
    strr = time.ctime(os.path.getmtime(file_name))
    year = strr[-4:]
    month = strr[4:7]
    return year, month
    
def chkMkDir(name):
    if os.path.isdir(name):
        #print(name +" directory exists")
        pass
    else:
        os.mkdir(name)

#iterate current folder to get the folder name which needs to be sorted
for itm in os.listdir():
    if os.path.isdir(itm):
        src = itm
        #print("directory:",itm)
        break

#target folder to place sorted files.
sort_folder = src + "_sort"

# create if not found [needed when u run second time ;)]
if os.path.isdir(sort_folder) == False:
    os.mkdir(sort_folder)

# get target dir name
os.chdir(sort_folder)
target = os.getcwd()

# back to folder to sort
os.chdir(source)
os.chdir(src)

# store source folder 
src_path = os.getcwd()
#print(os.getcwd())

# iterate source folder 
for subdir, dirs, files in os.walk(src_path):
    for itm in files:
        #print("item :", itm, "curr dir: ", os.getcwd())
        count += 1
        #get item's location, folder
        filepath = subdir + os.sep
        #print("File's location: ",filepath, "Curr dir: ", os.getcwd())
        os.chdir(filepath)
        #print("Curr dir: ", os.getcwd())

        # get year and month
        year, mon = 0, ""
        yr, mon = getYrMon(itm)
        os.chdir(target)
        #print(os.getcwd())

        #check if directory exists
        chkMkDir(yr)

        # move to year directory
        os.chdir(yr)
        #print(os.getcwd())

        #check if month directory exists
        chkMkDir(mon)

        # move to month directory
        os.chdir(mon)

        # get target folder location 
        dst = os.getcwd()
        
        #print("dest dir: ", dst)

        # check if file exits in target directory
        if (os.path.exists(itm)):
            #print("File Exists")
            fe = True
        else:
            fe = False

        # move to file source directory
        os.chdir(filepath)

        # if file exists, rename file with rand int number [1-100] range
        # and add infront of file name with _ with file name
        if fe == True :
            if skip == True:
                print(itm,"- File exists fldr: ",dst, " SKIP enabled ")
            else:
                itm_n = str(random.randint(1,100))+"_"+ itm
                #print(itm_n)
                os.rename(itm, itm_n)
                print(itm, " : file exists, REN, COPY as skip disabled ",itm_n, " to ",dst)
                shutil.move(itm_n,dst)
        else:
            print("copying ",itm, "to",dst)
            shutil.move(itm,dst)

print(count," : files processed ")
